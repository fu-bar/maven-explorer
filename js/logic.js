const sourceToTargets = {}
const targetToSources = {}

function parseNode(node) {
    let parsed = {}
    let parts = node.split(':')
    parsed['groupId'] = parts[0]
    parsed['artifactId'] = parts[1]
    parsed['type'] = parts[2]
    parsed['version'] = parts[3]
    return parsed;
}

function deduplicateArrayValues(mapping) {
    let keys = Object.keys(mapping);
    keys.forEach((key) => {
        mapping[key] = mapping[key].filter((v, i, a) => a.findIndex(v2 => (JSON.stringify(v2) === JSON.stringify(v))) === i)
    });
}

function processFile(fileReadEvent) {
    const regexp = new RegExp('"(.*?)" -> "(.*?)" ;', 'g')
    const fileContent = fileReadEvent.target.result;

    const matches = fileContent.matchAll(regexp)
    for (const match of matches) {
        let source = parseNode(match[1]);
        let target = parseNode(match[2]);
        const sourceKey = JSON.stringify(source)
        const targetKey = JSON.stringify(target)
        if (!(sourceKey in sourceToTargets)) {
            sourceToTargets[sourceKey] = []
        }
        if (!(targetKey in targetToSources)) {
            targetToSources[targetKey] = []
        }
        sourceToTargets[sourceKey].push(target)
        targetToSources[targetKey].push(source)
    }
    deduplicateArrayValues(sourceToTargets)
    deduplicateArrayValues(targetToSources)

    if (Object.keys(targetToSources).length === 0) {
        alert("Oops!\nAre you sure it's a dot-graph file?")
    }

    let allTargets = []
    Object.keys(targetToSources).forEach((key) => {
        allTargets.push(JSON.parse(key))
    })

    allTargets.sort(function (a, b) {
        return (+(a.groupId > b.groupId) || +(a.groupId === b.groupId) - 1) || (+(a.artifactId > b.artifactId) || +(a.artifactId === b.artifactId) - 1) || (+(a.version > b.version) || +(a.version === b.version) - 1);
    });

    populateTable('allArtifactsTable', allTargets)

    console.log("Finished processing")

}

function populateTable(tableId, items) {
    let tableBody = $(`#${tableId}`).find('tbody');
    if (items == null) {
        console.log(`No items for ${tableId}`)
        return;
    }
    items.forEach((item) => {
        tableBody.append(`<tr><th>${item['groupId']}<td>${item['artifactId']}</td><td>${item['type']}</td><td>${item['version']}</td></th></tr>`)
    });
}

function presentInfo(groupId, artifactId, type, version) {
    let selectionStringRepresentation = groupId + ':' + artifactId + ':' + type + ':' + version;
    $('#examinedElement').text(selectionStringRepresentation)
    $("#incomingLinks tbody tr").remove();
    $("#outgoingLinks tbody tr").remove();
    let key = JSON.stringify(parseNode(selectionStringRepresentation));
    let sourcesOfSelection = targetToSources[key];
    let targetsOfSelection = sourceToTargets[key]
    populateTable('incomingLinks', sourcesOfSelection)
    populateTable('outgoingLinks', targetsOfSelection)
}

$(document).ready(function () {
    // Bind filtering operation to the filtering text input
    $("#artifactFilter").on("keyup", function () {
        const value = $(this).val().toLowerCase();
        $("#artifactsTable tr").filter(function () {
            $(this).toggle($(this).text().toLowerCase().indexOf(value) > -1)
        });
    });

    // Make all table rows clickable
    ['artifactsTable', 'incomingLinks', 'outgoingLinks'].forEach(tableName => {
        $(`#${tableName}`).on('click', 'tr', function () {
            let groupId = this.cells[0].innerHTML
            let artifactId = this.cells[1].innerHTML
            let type = this.cells[2].innerHTML
            let version = this.cells[3].innerHTML
            presentInfo(groupId, artifactId, type, version)
        });
    });

    const dropZone = document.getElementById('dropZone');

    // Get file data on drop
    dropZone.addEventListener('drop', function (e) {
        e.stopPropagation();
        e.preventDefault();
        let files = e.dataTransfer.files; // Array of all files
        if (files.length>1) {
            alert("Oops!\nOnly 1 file is supported at this time.")
            return;
        }
        let file = files[0]
        const reader = new FileReader();
        reader.onload = processFile
        reader.readAsText(file);
    });
});